import React from 'react';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import PokemonContainer from '@modules/pokemon/containers/pokemonList';
import MainLayout from '@components/Layouts/Main';

export default function Index() {
  return (
    <MainLayout title="Pokemon List - My Awesome Pokemon">
      <Box>
        <Typography variant="h4" component="h1" gutterBottom>
          Pokemon List
        </Typography>
        <PokemonContainer />
      </Box>
    </MainLayout>
  );
}
