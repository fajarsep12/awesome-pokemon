import React from 'react';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import MainLayout from '@components/Layouts/Main';

export default function Index() {
  return (
    <MainLayout title="Detail - My Awesome Pokemon">
      <Box>
        <Typography variant="h4" component="h1" gutterBottom>
          Detail pokemon
        </Typography>
      </Box>
    </MainLayout>
  );
}
