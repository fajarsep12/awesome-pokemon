const goTop = (event: React.MouseEvent<HTMLDivElement>) => {
  const anchor = (
    (event.target as HTMLDivElement).ownerDocument || document
  ).querySelector('#top-anchor');
  if (anchor) {
    anchor.scrollIntoView({ behavior: 'smooth', block: 'center' });
  }
};

export default goTop;
