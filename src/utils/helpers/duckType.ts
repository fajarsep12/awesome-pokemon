export const generateActions = (prefix: string, action: string) => ({
  REQUEST: `${prefix}/${action}_REQUEST`,
  SUCCESS: `${prefix}/${action}_SUCCESS`,
  FAILURE: `${prefix}/${action}_FAILURE`
});
