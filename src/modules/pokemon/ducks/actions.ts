import { createAction } from 'redux-actions';
import { GET_POKEMON } from './constants';

export const getPokemon = {
  request: createAction(GET_POKEMON.REQUEST, ({ page }) => page),
  success: createAction(GET_POKEMON.SUCCESS),
  failure: createAction(GET_POKEMON.FAILURE)
};
