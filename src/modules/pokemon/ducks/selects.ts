import { createSelector } from 'reselect';

const selectPokemon = () => state => state.pokemon;

export const selectPokemonList = () =>
  createSelector(selectPokemon(), state => state.pokemonList);
