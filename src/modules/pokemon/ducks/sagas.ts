import { call, takeLatest, put, all } from 'redux-saga/effects';

import { getPokemon } from './actions';
import { GET_POKEMON } from './constants';
import { PokemonResponse } from '../types/pokemon';
import * as pokeService from '../services';

function* getPokemonSaga({ payload: page }: any) {
  try {
    const res: PokemonResponse = yield call(pokeService.getPokemonList, page);
    if (res.status === 200) {
      const { count, next, previous, results } = res.data;

      yield put(
        getPokemon.success({
          next,
          previous,
          count,
          pokemons: results
        })
      );
      return;
    }

    yield put(getPokemon.failure({ message: res.status }));
  } catch (error) {
    yield put(getPokemon.failure({ message: error }));
  }
}

export default function* pokemonSaga() {
  yield all([takeLatest(GET_POKEMON.REQUEST, getPokemonSaga)]);
}
