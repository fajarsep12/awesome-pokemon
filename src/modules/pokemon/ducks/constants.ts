import { generateActions } from '@utils/helpers/duckType';

export const GET_POKEMON = generateActions('app', 'GET_POKEMON');
export const GET_POKEMON_DETAIL = generateActions('app', 'GET_POKEMON_DETAIL');
