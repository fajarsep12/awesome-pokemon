import { handleActions } from 'redux-actions';
import { combineReducers } from 'redux';
import { GET_POKEMON } from './constants';
import { IInitialState } from '../types/pokemon';

const initialState: IInitialState = {
  pokemonList: {
    isFetching: false,
    count: 0,
    next: null,
    previous: null,
    pokemons: [],
    message: ''
  }
};

const pokemonReducer = handleActions(
  {
    [GET_POKEMON.REQUEST]: state => ({ ...state, isFetching: true }),
    [GET_POKEMON.SUCCESS]: (
      state,
      { payload: { next, previous, pokemons, count } }
    ) => ({ ...state, next, pokemons, count, previous, isFetching: false }),
    [GET_POKEMON.FAILURE]: (state, { payload: { message } }) => ({
      ...state,
      message
    })
  },
  initialState.pokemonList
);

export default combineReducers({
  pokemonList: pokemonReducer
});
