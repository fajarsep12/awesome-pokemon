import React, { useCallback, useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Box } from '@material-ui/core';
import Pagination from 'material-ui-flat-pagination';

import { getPokemon } from '../../ducks/actions';
import { selectPokemonList } from '../../ducks/selects';
import { PokemonsState } from '../../types/pokemon';

import ItemList from './ItemList';
import ListLoading from '@components/Loading/List';

function PokemonContainer() {
  const dispatch = useDispatch();
  const refToTop = useRef(null);
  const [page, setPage] = useState({
    limit: 20,
    offset: 0
  });
  const pokemon: PokemonsState = useSelector(selectPokemonList());

  useEffect(() => {
    handleFetchPokemon(page);
  }, [page]);

  const handleChangePage = useCallback((_e, offset) => {
    setPage({
      offset,
      limit: 20
    });
    scrollTorefToTop();
  }, []);

  const handleFetchPokemon = useCallback(
    (params: any) => {
      dispatch(getPokemon.request({ page: params }));
    },
    [dispatch, page]
  );

  const scrollTorefToTop = () => {
    window.scrollTo(0, (refToTop as any).offsetTop);
  };

  if (pokemon.isFetching) {
    return <ListLoading />;
  }

  return (
    <div ref={refToTop}>
      {pokemon && (
        <Box>
          <ItemList pokemons={pokemon.pokemons} />
          <Box textAlign="center">
            <Pagination
              size="medium"
              limit={page.limit}
              offset={page.offset}
              total={pokemon.count}
              onClick={handleChangePage}
              reduced
            />
          </Box>
        </Box>
      )}
    </div>
  );
}

export default PokemonContainer;
