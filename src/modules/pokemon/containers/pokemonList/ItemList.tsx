import React from 'react';
import { Card, Box, Typography } from '@material-ui/core';
import { Pokemons } from '../../types/pokemon';

interface Props {
  pokemons: Array<Pokemons>;
}

function ItemList(props: Props) {
  const { pokemons } = props;
  return (
    <Box>
      {pokemons.map(item => (
        <Box key={Math.random()} my={1}>
          <Card>
            <Box px={2} py={2}>
              <Typography>{item.name}</Typography>
              <Typography variant="caption">owned</Typography>
            </Box>
          </Card>
        </Box>
      ))}
    </Box>
  );
}

export default ItemList;
