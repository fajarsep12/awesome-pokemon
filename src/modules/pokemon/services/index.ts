import { Api } from '@utils/api';

const _api = new Api();

export const getPokemonList = ({ offset, limit }: any) => {
  return _api.get(`/pokemon?offset=${offset}&limit=${limit}`);
};
