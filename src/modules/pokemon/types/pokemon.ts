import { AxiosResponse } from 'axios';

export interface PokemonsResult {
  count: number;
  next: any;
  previous?: any;
  results?: Array<Pokemons>;
}

export interface Pokemons {
  name: string;
  url: string;
}

export type PokemonResponse = AxiosResponse<PokemonsResult>;

export interface PokemonsState extends PokemonsResult {
  isFetching: boolean;
  message: string;
  pokemons: Array<Pokemons>;
}

export interface IInitialState {
  pokemonList: PokemonsState;
}
