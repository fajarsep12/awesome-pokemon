import { combineReducers } from 'redux';
import pokemon from '@modules/pokemon/ducks/reducers';

const reducers = combineReducers({
  pokemon
});

export default reducers;
