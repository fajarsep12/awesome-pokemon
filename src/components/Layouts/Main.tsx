import React from 'react';
import Head from 'next/head';

import { Container, Box } from '@material-ui/core';
import AppBar from '../AppBar';
import BottomNav from '../BottomNav';

type Props = {
  children: React.ReactNode;
  title: string;
};

function MainLayout(props: Props) {
  const { children, title } = props;
  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <AppBar children={<div />} />
      <Box>
        <Container style={{ marginTop: 100, marginBottom: 100 }}>
          {children}
        </Container>
      </Box>

      <BottomNav />
    </div>
  );
}

export default MainLayout;
