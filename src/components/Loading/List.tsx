import React from 'react';
import { Box, Card } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';

function ListLoading() {
  return (
    <Box>
      {[...new Array(6)].map(i => (
        <Box my={1} key={i}>
          <Card>
            <Box px={2} py={2.5}>
              <Skeleton variant="text" width="50%" />
              <Skeleton variant="text" width="20%" />
            </Box>
          </Card>
        </Box>
      ))}
    </Box>
  );
}

export default ListLoading;
