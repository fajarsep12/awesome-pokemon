import React, { useEffect } from 'react';
import Router, { useRouter } from 'next/router';
import { BottomNavigation, BottomNavigationAction } from '@material-ui/core';
import { bottomMenu } from './menu';

function BottomNav() {
  const router = useRouter();
  const [value, setValue] = React.useState('/');

  useEffect(() => {
    setValue(router.asPath);
  }, []);

  const handleChangeNav = (_event: React.ChangeEvent<{}>, newValue: string) => {
    setValue(newValue);
    Router.push(newValue);
  };

  return (
    <BottomNavigation
      value={value}
      onChange={handleChangeNav}
      style={{
        width: '100%',
        position: 'fixed',
        bottom: 0
      }}
    >
      {bottomMenu.map(({ icon, label, valueItem }) => (
        <BottomNavigationAction
          key={Math.random()}
          label={label}
          value={valueItem}
          icon={<img width="30" src={`/svg/${icon}`} />}
        />
      ))}
    </BottomNavigation>
  );
}

export default BottomNav;
