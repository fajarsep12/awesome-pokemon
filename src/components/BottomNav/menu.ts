export const bottomMenu = [
  { label: 'Pokemon List', valueItem: '/', icon: 'smartphone.svg' },
  { label: 'My Pokemon', valueItem: '/pokemon/mine', icon: 'pokeballs.svg' }
];
