import React from 'react';
import {
  AppBar,
  Toolbar,
  useScrollTrigger,
  Typography
} from '@material-ui/core';

type Props = {
  children: React.ReactElement;
  window?: () => Window;
};

function ElevationScroll(props: Props) {
  const { children, window } = props;

  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window ? window() : undefined
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0
  });
}

function MainAppBar(props: Props) {
  return (
    <React.Fragment>
      <ElevationScroll {...props}>
        <AppBar>
          <Toolbar>
            <Typography variant="h6">My Awesome Pokemon</Typography>
          </Toolbar>
        </AppBar>
      </ElevationScroll>
    </React.Fragment>
  );
}

export default MainAppBar;
